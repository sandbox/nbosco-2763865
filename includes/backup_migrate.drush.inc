<?php


/**
 * @file
 * Drush commands for Updato.
 */
 
/**
 * Implementation of hook_drush_command().
 */
function updato_drush_command() {
  $items['bam-backup'] = array(
    'callback' => 'updato_drush_backup',
    'description' => dt('Backup the site\'s database with Updato.'),
    'aliases' => array('bb'),
    'examples' => array(
      'drush bam-backup' => 'Backup the default databse to the manual backup directory using the default settings.', 
      'drush bam-backup db scheduled mysettings' => 'Backup the database to the scheduled directory using a settings profile called "mysettings"', 
      'drush bam-backup files' => 'Backup the files directory to the manual directory using the default settings. The Updato Files module is required for files backups.', 
    ),
    'arguments' => array(
      'source'        => "Optional. The id of the source (usually a database) to backup. Use 'drush bam-sources' to get a list of sources. Defaults to 'db'",
      'destination'   => "Optional. The id of destination to send the backup file to. Use 'drush bam-destinations' to get a list of destinations. Defaults to 'manual'",
      'profile'       => "Optional. The id of a settings profile to use. Use 'drush bam-profiles' to get a list of available profiles. Defaults to 'default'",
    ),
  );
  $items['bam-restore'] = array(
    'callback' => 'updato_drush_restore',
    'description' => dt('Restore the site\'s database with Updato.'),
    'arguments' => array(
      'source'        => "Required. The id of the source (usually a database) to restore the backup to. Use 'drush bam-sources' to get a list of sources. Defaults to 'db'",
      'destination'   => "Required. The id of destination to send the backup file to. Use 'drush bam-destinations' to get a list of destinations. Defaults to 'manual'",
      'backup id'     => "Required. The id of a backup file restore. Use 'drush bam-backups' to get a list of available backup files.",
    ),
    'options' => array(
      'yes' => 'Skip confirmation',
    ),
  );
  $items['bam-destinations'] = array(
    'callback' => 'updato_drush_destinations',
    'description' => dt('Get a list of available destinations.'),
  );

  $items['bam-sources'] = array(
    'callback' => 'updato_drush_sources',
    'description' => dt('Get a list of available sources.'),
  );
  $items['bam-profiles'] = array(
    'callback' => 'updato_drush_profiles',
    'description' => dt('Get a list of available settings profiles.'),
  );
  $items['bam-backups'] = array(
    'callback' => 'updato_drush_destination_files',
    'description' => dt('Get a list of previously created backup files.'),
    'arguments' => array(
      'destination'   => "Optional. The id of destination to list backups from. Use 'drush bam-destinations' to get a list of destinations.",
    ),
  );
  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function updato_drush_help($section) {
  switch ($section) {
    case 'drush:bam-backup':
      return dt("Backup the site's database using default settings.");
    case 'drush:bam-restore':
      return dt('Restore the site\'s database with Updato.');
    case 'drush:bam-destinations':
      return dt('Get a list of available destinations.');
    case 'drush:bam-profiles':
      return dt('Get a list of available settings profiles.');
    case 'drush:bam-backups':
      return dt('Get a list of previously created backup files.');
  }
}

/**
 * Backup the default database.
 */
function updato_drush_backup($source_id = 'db', $destination_id = 'manual', $profile_id = 'default') {
  updato_include('profiles', 'destinations', 'sources');

  // Set the message mode to logging.
  _updato_message_callback('_updato_message_drush');

  if (!updato_get_source($source_id)) {
    _updato_message("Could not find the source '@source'. Try using 'drush bam-sources' to get a list of available sources or use 'db' to backup the Drupal database.", array('@source' => $source_id), 'error');
    return;
  }
  if (!updato_get_destination($destination_id)) {
    _updato_message("Could not find the destination '@destination'. Try using 'drush bam-destinations' to get a list of available destinations.", array('@destination' => $destination_id), 'error');
    return;
  }
  $settings = updato_get_profile($profile_id);
  if(!$settings) {
    _updato_message("Could not find the profile '@profile'. Try using 'drush bam-profiles' to get a list of available profiles.", array('@profile' => $profile_id), 'error');
    return;
  }

  _updato_message('Starting backup...');
  $settings->destination_id = $destination_id;
  $settings->source_id = $source_id;
  updato_perform_backup($settings);
}

/**
 * Restore to the default database.
 */
function updato_drush_restore($source_id = '', $destination_id = '', $file_id = '') {

  updato_include('profiles', 'destinations', 'sources');

  // Set the message mode to drush output.
  _updato_message_callback('_updato_message_drush');

  if (!updato_get_source($source_id)) {
    _updato_message("Could not find the source '@source'. Try using 'drush bam-sources' to get a list of available sources or use 'db' to backup the Drupal database.", array('@source' => $source_id), 'error');
    return;
  }
  if (!$destination = updato_get_destination($destination_id)) {
    _updato_message("Could not find the destination '@destination'. Try using 'drush bam-destinations' to get a list of available destinations.", array('@destination' => $destination_id), 'error');
    return;
  }
  else if (!$file_id || !$file = updato_destination_get_file($destination_id, $file_id)) {
    _updato_message("Could not find the file '@file'. Try using 'drush bam-backups @destination' to get a list of available backup files in this destination destinations.", array('@destination' => $destination_id, '@file' => $file_id), 'error');
    return;
  }

  drush_print(dt('Restoring will delete some or all of your data and cannot be undone. ALWAYS TEST YOUR BACKUPS ON A NON-PRODUCTION SERVER!'));
  if (!drush_confirm(dt('Are you sure you want to perform the restore?'))) {
    return drush_user_abort();
  }
  _updato_message('Starting restore...');
  $settings = array('source_id' => $source_id);
  updato_perform_restore($destination_id, $file_id, $settings);
}

/**
 * Get a list of available destinations.
 */
function updato_drush_destinations() {
  return _updato_drush_destinations('all');
}

/**
 * Get a list of available sources.
 */
function updato_drush_sources() {
  return _updato_drush_sources('source');
}


/**
 * Get a list of available destinations with the given op.
 */
function _updato_drush_destinations($op = NULL) {
  updato_include('destinations');
  $rows = array(array(dt('ID'), dt('Name'), dt('Operations')));
  foreach (updato_get_destinations($op) as $destination) {
    $rows[] = array(
      $destination->get_id(),
      $destination->get_name(),
      implode (', ', $destination->ops()),
    );
  }
  drush_print_table($rows, TRUE, array(32, 32));
}


/**
 * Get a list of available destinations with the given op.
 */
function _updato_drush_sources($op = NULL) {
  updato_include('sources');
  $rows = array(array(dt('ID'), dt('Name'), dt('Operations')));
  foreach (updato_get_sources($op) as $destination) {
    $rows[] = array(
      $destination->get_id(),
      $destination->get_name(),
      implode (', ', $destination->ops()),
    );
  }
  drush_print_table($rows, TRUE, array(32, 32));
}

/**
 * Get a list of available profiles.
 */
function updato_drush_profiles() {
  updato_include('profiles');
  $rows = array(array(dt('ID'), dt('Name')));
  foreach (updato_get_profiles() as $profile) {
    $rows[] = array(
      $profile->get_id(),
      $profile->get_name(),
    );
  }
  drush_print_table($rows, TRUE, array(32, 32));
}

/**
 * Get a list of files in a given destination
 */
function updato_drush_destination_files($destination_id = NULL) {
  updato_include('destinations');
  $destinations = array();

  // Set the message mode to drush output.
  _updato_message_callback('_updato_message_drush');
  if ($destination_id && !$destination = updato_get_destination($destination_id)) {
    _updato_message("Could not find the destination '@destination'. Try using 'drush bam-destinations' to get a list of available destinations.", array('@destination' => $destination_id), 'error');
    return;
  }

  // Single destination required.
  if ($destination) {
    $destinations = array($destination);
  }
  // List all destinations
  else {
    $destinations = updato_get_destinations('list files');
  }

  // Load all the files.
  $rows = $sort = array();
  foreach ($destinations as $destination) {
    $destination->file_cache_clear();
    $dest_files = $destination->list_files();
    foreach ($dest_files as $id => $file) {
      $info = $file->info();
      $rows[] = array(
        check_plain($info['filename']),
        $destination->get_id(),
        format_date($info['filetime'], 'small'),
        format_interval(time() - $info['filetime'], 1),
        format_size($info['filesize']),
      );
      $sort[] = $info['filetime'];
    }
  }

  $headers = array(array(
    dt('Filename'),
    dt('Destination'),
    dt('Date'),
    dt('Age'),
    dt('Size'),
  ));

  if (count($rows)) {
    array_multisort($sort, SORT_DESC, $rows);
    drush_print_table(array_merge($headers, $rows), TRUE);
  }
  else {
    drush_print(dt('There are no backup files to display.'));
  }
}

/**
 * Send a message to the drush log.
 */
function _updato_message_drush($message, $replace, $type) {
  // If this is an error use drush_set_error to notify the end user and set the exit status
  if ($type == 'error') {
    drush_set_error(strip_tags(dt($message, $replace)));
  }
  else {
    // Use drush_log to display to the user.
    drush_log(strip_tags(dt($message, $replace)), str_replace('status', 'notice', $type));
  }
  // Watchdog log the message as well for admins.
  _updato_message_log($message, $replace, $type);
}
